library bepo_learning;

import 'dart:html' as html;
import 'dart:async';
import 'dart:convert' as convert;
import 'package:stagexl/stagexl.dart';

part 'source/key_button.dart';
part 'source/keyboard.dart';
part 'source/text_box.dart';

Stage stage;
RenderLoop renderLoop;
ResourceManager resourceManager;
Keyboard keyboard;

void main() {
     
  var canvas = html.querySelector('#stage');
  stage = new Stage("myStage", canvas, 960, 570);
  stage.scaleMode = StageScaleMode.SHOW_ALL;
  stage.align = StageAlign.NONE;

  renderLoop = new RenderLoop();
  renderLoop.addStage(stage);
  
  resourceManager = new ResourceManager()
    ..addTextFile("bepo", "json/bepo.json")   
    ..load().then((result) {
      var bepoData = convert.JSON.decode(resourceManager.getTextFile("bepo"));      
      keyboard = new Keyboard(bepoData);
      stage.addChild(keyboard);
    });
  html.window.onKeyDown.listen(keyDownListener);  
  html.window.onKeyUp.listen(keyUpListener);
  html.window.onKeyPress.listen(keyPressListener);
  
}

void keyPressListener(html.KeyboardEvent e) {
  keyboard.animate(e);
  //keyboard.unanimate(e.keyCode);
}

void keyDownListener(html.KeyboardEvent e) {
  //    keyboard.animate(e);
}

void keyUpListener(html.KeyboardEvent e) {
  
    keyboard.unanimatePrevious();
}