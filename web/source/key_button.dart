part of bepo_learning;

class KeyButton extends Sprite {
  
  final int DEFAULT_WIDTH = 2 * Keyboard.DEFAULT_WIDTH;
  final int DEFAULT_HEIGHT = 2 * Keyboard.DEFAULT_HEIGHT;
  
  Map keyInfo;
  TextField keyTextField;
  
  KeyButton(this.keyInfo) {
    var x = keyInfo["x"] as int;
    var y = keyInfo["y"] as int;
    var keyCode = keyInfo["keyCode"] as int;
    var shiftKeyCode = keyInfo["shift_keyCode"] as int;    
    var displayText = keyInfo["display"] as String;
    var infoWidth = keyInfo["width"] as int;
    if (infoWidth == null) {
      infoWidth =1;
    }
    var infoHeight = keyInfo["height"] as int;
    if (infoHeight == null) {
      infoHeight =1;
    }
    
    this.pivotX = DEFAULT_WIDTH * infoWidth;
    this.pivotY = Keyboard.DEFAULT_HEIGHT;
    this.scaleX = 0.5;
    this.scaleY = 0.5;
    this.useHandCursor = true;
    this.mouseChildren = false;

    this.graphics.beginPath();
    this.graphics.rectRound(6, 6,  (DEFAULT_WIDTH - 12)  + ( DEFAULT_WIDTH *  (infoWidth -1)) , (DEFAULT_HEIGHT - 12) + (DEFAULT_HEIGHT  * (infoHeight-1)) , 8, 8);
    this.graphics.closePath();
    this.graphics.fillColor(_getFillColor(x,y));
    this.graphics.strokeColor(Color.Black, 1);
    
    String font =  "Open Sans, Helvetica Neue, Helvetica, Arial, sans-serif";
    var symbolTextFormat = new TextFormat(font, 40, Color.Black, bold:true);

    keyTextField = new TextField()
      ..defaultTextFormat = symbolTextFormat
      ..x = 0
      ..y = 20
      ..width = DEFAULT_WIDTH
      ..cacheAsBitmap = false
      ..autoSize = TextFieldAutoSize.CENTER
      ..mouseEnabled = false
      ..text = displayText;

    addChild(keyTextField);
    
    applyCache(0, 0, DEFAULT_WIDTH * infoWidth , DEFAULT_HEIGHT);
  }
  
  animateTo(num scale, num alpha) {
    this.stage.juggler.removeTweens(this);
    this.stage.juggler.tween(this, 0.15, TransitionFunction.easeOutQuadratic)
      ..animate.scaleX.to(scale)
      ..animate.scaleY.to(scale)
      ..animate.alpha.to(alpha);
    
  }
  int _getFillColor(int x, int y) {
    switch (x) {
      case 0 :
        return Color.Red;
      case 1 : 
        return Color.Red;
      case 2 :
        return Color.Orange;
      case 3 : 
        return Color.Yellow;
      case 4 :
        return Color.Chartreuse;
      case 5 : 
        return Color.Chartreuse;
      case 6 : 
        return Color.WhiteSmoke;
      case 7:
        return y == 5 ? Color.WhiteSmoke : Color.Aqua;
      case 8:
        return Color.Aqua;
      case 9:
        return Color.BlueViolet;
      case 10:
        return Color.PaleVioletRed;
      case 11: 
        return Color.Wheat;
      case 12: 
        return Color.Wheat;
      case 13:
        return Color.Wheat;
      default: 
        return Color.WhiteSmoke;
        
      
    }
  }
}