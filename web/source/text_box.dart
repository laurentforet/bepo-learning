part of bepo_learning;

class TextBox extends DisplayObjectContainer {
  
    String _content;
    TextField _contentText;
    
    TextBox() {
      _contentText = new TextField();
    }
  
    onKeyPress(html.KeyboardEvent event) {
      
      
    }
    
}

class KeyInfoBox extends DisplayObjectContainer {
  
    html.KeyboardEvent keyboardEvent;
  
    KeyInfoBox(this.keyboardEvent) {
      _addDataTextField("charCode : ${keyboardEvent.charCode}", 20, 44);
      _addDataTextField("keyCode : ${keyboardEvent.keyCode}", 20, 64);
      _addDataTextField("eventPhase : ${keyboardEvent.eventPhase}", 20, 84);
      _addDataTextField("keyLocation : ${keyboardEvent.keyLocation}", 20, 104);
      _addDataTextField("layer : ${keyboardEvent.layer.x}, ${keyboardEvent.layer.y}", 20, 124);
      _addDataTextField("location : ${keyboardEvent.location}", 20, 144);
      _addDataTextField("page : ${keyboardEvent.page.x}, ${keyboardEvent.page.y}", 20, 164);
      _addDataTextField("type : ${keyboardEvent.type}", 20, 184);
      _addDataTextField("which : ${keyboardEvent.which}", 20, 204);
      _addDataTextField("shift + alt + altgr : ${keyboardEvent.shiftKey} + ${keyboardEvent.altKey} + ${keyboardEvent.altGraphKey}", 20, 224);
      
    }
    
    _addDataTextField(String text, int x, int y) {
      var font =  "Open Sans, Helvetica Neue, Helvetica, Arial, sans-serif";
      var dataTextFormat = new TextFormat(font, 14, Color.Black);

      var dataTextField = new TextField()
      ..defaultTextFormat = dataTextFormat
      ..x = x
      ..y = y
      ..cacheAsBitmap = false
      ..autoSize = TextFieldAutoSize.LEFT
      ..text = text;

      addChild(dataTextField);
  }
}