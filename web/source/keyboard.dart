part of bepo_learning;

class Keyboard extends DisplayObjectContainer {
  
    Map keyboardData;
    static const DEFAULT_WIDTH = 50;
    static const DEFAULT_HEIGHT = 50;
    KeyButton _currentKeyPress;
    
    Map<int, KeyButton> keyCodeMap = new Map();
    
    DisplayObject _detail = null;
    Juggler _juggler = stage.juggler;
  
    Keyboard(this.keyboardData) {
       _addKeys();
    }
    
    _addKeys() {
      for(var keyInfo in this.keyboardData["keys"]) {
        var x = keyInfo["x"] as int;
        var y = keyInfo["y"] as int;
        var keyCode = keyInfo["keyCode"] as int;
        var shiftKeyCode = keyInfo["shift_keyCode"] as int;
        
        var keyButton = new KeyButton(keyInfo);
        keyButton.x =  x * DEFAULT_WIDTH + 150;
        keyButton.y =  y * DEFAULT_HEIGHT + 250;
        keyCodeMap[keyCode] = keyButton;
        keyCodeMap[shiftKeyCode] = keyButton;
        
        addChild(keyButton);
      }
    }
    
    animate(html.KeyboardEvent keyBoardEvent) {
      unanimatePrevious();
      
      KeyButton keyButton = keyCodeMap[keyBoardEvent.keyCode];
      if (keyButton != null) {
        keyButton.animateTo(0.7, 1.0);
        _currentKeyPress = keyButton;
      }
      _detail = new KeyInfoBox(keyBoardEvent);
      _detail.x = 150;
      _detail.y = 10;
      _detail.alpha = 1.0;
      _detail.addTo(this);
       
    }
    
    unanimatePrevious() {
      //var keyButton = keyCodeMap[keyCode];
      if (_currentKeyPress != null) {
        _currentKeyPress.animateTo(0.5, 1.0);
        _currentKeyPress = null;
      }
      if (_detail != null ) {
        _detail.removeFromParent();
        _detail = null;
      }
      
      //if (_detail != null) {
      //  _juggler.tween(_detail, 0.3, TransitionFunction.linear)
      //    ..animate.alpha.to(0.0)
      //    ..onComplete = _detail.removeFromParent;
      //  _detail = null;
      //}
    }
}